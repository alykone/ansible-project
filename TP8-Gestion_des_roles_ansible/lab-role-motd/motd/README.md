Rôle motd 
========= 
 
Ce rôle a pour but de copier un fichier /etc/motd sur les hôtes gérés. 
 
Variables de rôle 
----------------- 
Variable : 'nom_projet' 
Elle spécifie le nom du projet. 
Valeur valide : chaîne de caractères (string). 
 
Exemple de Playbook 
------------------- 
    --- 
    - name: Création d'un rôle Ansible "motd" 
      hosts: servers 
 
      roles: 
        - role: motd 
 
Licence 
------- 
 
GPLv3
