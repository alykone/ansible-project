Objectifs du TP

Installer Ansible Engine sur le nœud de contrôle, préparer les hôtes à gérer et vérifier le bon fonctionnement

Taches a realiser

1- Vérifiez que vos machines sont à jour.

# dnf update -y

2- Installez Ansible sur la machine serveur1 (noeud de controle)

Installez EPEL (Extra Packages for Enterprise Linux) :

# dnf install epel-release

et ensuite:

# dnf install ansible

3- Installez le paquet python3 sur server1, server2 et server3

# dnf install python3 -y 

sur le noeud de controle et 

# dnf module install python36 -y

sur les hotes gerés toutes les machines

4- Affichez la version de Ansible Engine.

# ansible --version

5- Affichez la configuration du nœud de contrôle:

# ansible -m setup localhost

6- Utilisez le fichier inventaire par défaut.

le fichier par defaut de l'inventaire est /etc/ansible/hosts

7- Configurez server1 pour qu’elle se connecte en utilisant le protocole SSH sur les hôtes à gérer

On va creer la cle sur le noeud de control: 
# ssh-keygen
et le copier sur les hotes gerés:
# ssh-copy-id root@IP

8.Effectuez un ping depuis le nœud de contrôle vers les deux hôtes à gérer

# ansible -m ping all
 
