1. Les fichiers d’inventaire

Environnement nécessaire

Vous avez besoin de la machine virtuelle server1.staff.local.

Objectifs du TP

Concevoir des fichiers d’inventaire aux formats INI et YAML et convertir un fichier d’inventaire au format INI en YAML. Effectuer la vérification syntaxique des fichiers d’inventaire.

Tâches à réaliser

1.

Connectez-vous sur la machine server1 en tant que root.

2.

Créez le répertoire ~/inventaire et placez-vous dedans :

[root@server1 ~]# mkdir inventaire 
[root@server1 ~]# cd inventaire 
[root@server1 inventaire]# 
3.

Concevez le fichier d’inventaire inventory.ini au format INI :

Le groupe site_fr contient les groupes messagerie, web, database, ipa.

Les machines mailsrv01.lab.local, mailsrv02.lab.local, mailsrv03.lab.local sont membres du groupe messagerie.

Les machines appsrv01.web.local, lampsrv.web.local sont membres du groupe web.

Les machines dbsrv01.lab.local, dbsrv03.lab.local sont membres du groupe database.

La machine ipasrv.staff.local est membre du groupe ipa.

Fichier inventory.ini :

[messagerie] 
mailsrv0[1:3].lab.local 
 
[web] 
appsrv01.web.local 
lampsrv.web.local 
 
[database] 
dbsrv01.lab.local 
dbsrv03.lab.local 
 
[ipa] 
ipasrv.staff.local 
 
[site_fr:children] 
messagerie 
web 
database 
ipa 
4.

Concevez le fichier d’inventaire inv-hosts.yml au format YAML :

La machine proxy.staff.local n’appartient à aucun groupe.

Les machines websrv1.staff.local, websrv2.staff.local, websrv3.staff.local, websrv4.staff.local et dbsrv.staff.local sont membres du groupe appsrv.

Les machines ipasrv1.staff.local et ipasrv2.staff.local sont membres du groupe freeipa.

Fichier inv-hosts.yml :

--- 
all: 
 children: 
   ungrouped: 
     hosts: 
       proxy.staff.local: 
   appsrv: 
     hosts: 
       websrv[1:4].staff.local: 
       dbsrv.staff.local: 
   freeipa: 
     hosts: 
       ipasrv[1:2].staff.local: 
 

Vérifiez sa syntaxe :

[root@server1 inventaire]# yamllint inv-hosts.yml 
[root@server1 inventaire]# 
 

Si vous n’avez pas d’erreur, continuez, sinon corrigez.

5.

Convertissez le fichier d’inventaire inventory.ini au format YAML. Son nom doit être inventory.yml :

[root@server1 inventaire]# ansible-inventory -i inventory.ini -y \ 
> --list > inventory.yml 
[root@server1 inventaire]# 
 

Affichez le contenu du fichier inventory.yml :

[root@server1 inventaire]# catinventory.yml 
all: 
 children: 
   site_fr: 
     children: 
       database: 
         hosts: 
           dbsrv01.lab.local: {} 
           dbsrv03.lab.local: {} 
       ipa: 
         hosts: 
           ipasrv.staff.local: {} 
       messagerie: 
         hosts: 
           mailsrv01.lab.local: {} 
           mailsrv02.lab.local: {} 
           mailsrv03.lab.local: {} 
       web: 
         hosts: 
           appsrv01.web.local: {} 
           lampsrv.web.local: {} 
   ungrouped: {} 
[root@server1 inventaire]# 
 

Vérifiez sa syntaxe :

[root@server1 inventaire]# yamllint inventory.yml 
inventory.yml 
 1:1      warning  missing document start "---"  (document-start) 
 
[root@server1 inventaire]# 
 

Vous constatez un avertissement mais pas une erreur. Il manque les trois tirets "---" pour définir le début du fichier YML. Corrigez ceci :

[root@server1 inventaire]# sed -i '1i---' inventory.yml 
[root@server1 inventaire]# head -4 inventory.yml 
--- 
all: 
 children: 
   site_fr: 
[root@server1 inventaire]# 
 

Et vérifiez de nouveau :

[root@server1 inventaire]# yamllint inventory.yml 
[root@server1 inventaire]# 
2. Configurer ansible

Environnement nécessaire

Vous avez besoin de la machine virtuelle server1.staff.local. Ce TP est une suite du précédent TP Les fichiers d’inventaire.

Objectifs du TP

Le fichier de configuration de Ansible doit être /root/inventaire/ansible.cfg.

Tâches à réaliser

1.

Vous êtes connectés en tant que root sur l’hôte server1. Allez dans le répertoire inventaire.

[root@server1 ~]# cd inventaire 
[root@server1 inventaire]# 
2.

Vérifiez le fichier de configuration actuel :

[root@server1 inventaire]# ansible --version 
ansible 2.8.5 
  config file = /etc/ansible/ansible.cfg 
  configured module search path = ['/root/.ansible/plugins/modules', 
'/usr/share/ansible/plugins/modules'] 
  ansible python module location = /usr/lib/python3.6/site-packages/ansible 
  executable location = /usr/bin/ansible 
  python version = 3.6.8 (default, Oct  7 2019, 17:58:22) 
[GCC 8.2.1 20180905 (Red Hat 8.2.1-3)] 
[root@server1 inventaire]# 
 

Vous constatez que le fichier de configuration n’est pas /root/inventaire/ansible.cfg.

3.

Créez le fichier ansible.cfg dans le répertoire /root/inventaire dans lequel vous devez définir ces paramètres :

Le fichier inventaire à utiliser est inv-hosts.yml.

Le nom du compte utilisateur qui va servir à se connecter sur les hôtes à gérer est root.

Vous utiliserez l’authentification par clé publique.

Le changement de compte utilisateur après connexion sur l’hôte à gérer doit être actif.

L’utilisation de la méthode sudo est choisie pour effectuer le changement de compte.

Le compte utilisateur à utiliser est root.

Il faut désactiver la demande de mot de passe.

Le contenu du fichier /root/inventaire/ansible.cfg :

[defaults] 
inventory = ./inv-hosts.yml 
remote_user = root 
ask_pass = false 
 
[privilege_escalation] 
become = true 
become_method = sudo 
become_user = root 
become_ask_pass = false 
4.

Vérifiez que le fichier de configuration est bien /root/inventaire/ansible.cfg :

[root@server1 inventaire]# ansible --version 
ansible 2.8.5 
  config file = /root/inventaire/ansible.cfg 
  configured module search path = ['/root/.ansible/plugins/modules', 
'/usr/share/ansible/plugins/modules'] 
  ansible python module location = /usr/lib/python3.6/site-packages/ansible 
  executable location = /usr/bin/ansible 
  python version = 3.6.8 (default, Oct  7 2019, 17:58:22) 
[GCC 8.2.1 20180905 (Red Hat 8.2.1-3)] 
[root@server1 inventaire]# 
3. Utilisation des commandes ad hoc

Environnement nécessaire

Vous avez besoin des machines virtuelles server1.staff.local et server2.staff.local.

Ce TP est une suite des deux travaux pratiques précédents.

Objectifs du TP

Copier des fichiers depuis server1 vers server2 puis éteindre server2.

Tâches à réaliser

1.

Vous êtes connectés en tant que root sur l’hôte server1. Allez dans le répertoire inventaire.

[root@server1 ~]# cd inventaire 
[root@server1 inventaire]# 
 

Rappelez-vous que ce répertoire contient plusieurs fichiers d’inventaire et un fichier de configuration, lequel utilise inv-hosts.yml :

[root@server1 inventaire]# ls 
ansible.cfg  inventory.ini  inventory.yml  inv-hosts.yml 
[root@server1 inventaire]# 
2.

Créez un nouveau fichier d’inventaire /root/inventaire/machines.ini :

Créez un groupe servers.

Les hôtes server2 et server3 sont membres de ce groupe.

Fichier machines.ini :

[servers] 
server2 
server3 
3.

Vérifiez que server1 communique avec server2 en utilisant une commande ad hoc :

[root@server1 inventaire]# ansible server2 -m ping -i machines.ini 
server2 | SUCCESS => { 
   "ansible_facts": { 
       "discovered_interpreter_python": "/usr/libexec/platform-python" 
   }, 
   "changed": false, 
   "ping": "pong" 
} 
[root@server1 inventaire]# 
4.

Copiez des fichiers depuis server1 vers server2.

Créez un répertoire /root/inventaire/files :

[root@server1 inventaire]# mkdir files 
[root@server1 inventaire]# 
 

Créez un fichier ./files/info.txt qui contient la phrase « Un fichier texte. » :

[root@server1 inventaire]# echo "Un fichier texte." 1> files/info.txt 
[root@server1 inventaire]# 
 

Créez un second fichier ./files/information.txt qui contient la phrase « Un second fichier texte. » :

[root@server1 inventaire]# echo "Un second fichier texte." \  
> 1> files/information.txt 
[root@server1 inventaire]# 
 

Nous avons maintenant deux fichiers à copier dans le répertoire /root/data sur le server2.

Créez sur server2 le répertoire /root/data avec les droits 700. L’utilisateur propriétaire et le groupe propriétaire sont root :

[root@server1 inventaire]# ansible server2 -m file -a "dest=/root/data \ 
> mode=700 owner=root group=root state=directory" -i machines.ini 
server2 | CHANGED => { 
   "ansible_facts": { 
       "discovered_interpreter_python": "/usr/libexec/platform-python" 
   }, 
   "changed": true, 
   "gid": 0, 
   "group": "root", 
   "mode": "0700", 
   "owner": "root", 
   "path": "/root/data", 
   "secontext": "unconfined_u:object_r:admin_home_t:s0", 
   "size": 4096, 
   "state": "directory", 
   "uid": 0 
} 
[root@server1 inventaire]# 
 

Copiez les deux fichiers du répertoire ./files de server1 vers le répertoire /root/data sur server2 :

[root@server1 inventaire]# ansible server2 -m copy -a "src=./files/ \ 
> dest=/root/data" -i machines.ini 
server2 | CHANGED => { 
   "changed": true, 
   "dest": "/root/data/", 
   "src": "/root/inventaire/./files/" 
} 
[root@server1 inventaire]# 
 

Vérifiez que la copie s’est bien produite :

[root@server1 inventaire]# ansible server2 -m shell -a 'ls -l data' \ 
> -i machines.ini 
server2 | CHANGED | rc=0 >> 
total 8 
-rw-r--r--. 1 root root 25  4 janv. 00:51 information.txt 
-rw-r--r--. 1 root root 18  4 janv. 00:51 info.txt 
 
[root@server1 inventaire]# 
5.

Éteignez server2 dans un délai d’une minute avec le message « Fin du TP commandes ad hoc » :

[root@server1 inventaire]# ansible server2 -m shell -a "shutdown +1 \ 
> 'Fin du lab commandes ad hoc'" -i machines.ini 
server2 | CHANGED | rc=0 >> 
Shutdown scheduled for Sat 2020-01-04 01:08:56 CET, use 'shutdown -c' to cancel. 
 
[root@server1 inventaire]#
