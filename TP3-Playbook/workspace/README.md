Corrections du TP chapitre Playbooks
1. Installation de Apache HTTP Server

Environnement nécessaire

Vous avez besoin des trois machines virtuelles server1, server2 et server3.

Objectifs du TP

Vous devez installer Apache HTTP Server sur les hôtes server2 et server3 et ouvrir le port dans firewalld.

Tâches à réaliser

1.

Dans le répertoire de travail ~/workspace, un fichier inventaire machines.ini doit contenir le nom d’hôte des machines :

[root@server1 workspace]# cat machines.ini 
server2 
server3 
[root@server1 workspace]# 
 

La résolution de noms d’hôtes n’est pas assumée par un service DNS mais par le fichier /etc/hosts :

[root@server1 workspace]# cat /etc/hosts 
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6 
 
172.16.32.1                server2                server2.staff.local 
172.16.32.2                server3                server3.staff.local 
 
[root@server1 workspace]# 
2.

Dans le répertoire de travail ~/workspace, le fichier ansible.cfg définit :

le fichier inventaire à utiliser,

le compte root qui va servir à se connecter sur les hôtes gérés,

l’authentification par clé publique,

le changement de compte utilisateur après connexion sur l’hôte en utilisant root avec la méthode sudo,

la désactivation de la demande du mot de passe.

Le fichier ansible.cfg contient ces paramètres :

[root@server1 workspace]# cat ansible.cfg 
[defaults] 
inventory = ./machines.ini 
remote_user = root 
ask_pass = false 
 
[privilege_escalation] 
become = true 
become_method = sudo 
become_user = root 
become_ask_pass = false 
[root@server1 workspace]# 
 

Vérifiez la configuration utilisée par Ansible :

[root@server1 workspace]# ansible --version 
ansible 2.8.5 
  config file = /root/workspace/ansible.cfg 
  configured module search path = ['/root/.ansible/plugins/modules', 
'/usr/share/ansible/plugins/modules'] 
  ansible python module location = /usr/lib/python3.6/site-packages/ansible 
  executable location = /usr/bin/ansible 
  python version = 3.6.8 (default, Oct  7 2019, 17:58:22) [GCC 8.2.1 20180905 
(Red Hat 8.2.1-3)] 
[root@server1 workspace]# 
 

S’il prend bien ~/workspace/ansible.cfg comme fichier de configuration, poursuivez.

3.

Créez le fichier httpd.yml :

[root@server1 workspace]# cat httpd.yml 
--- 
- hosts: all 
 tasks: 
 - name: Installer les paquetages httpd 
   yum: name=httpd state=latest 
 - name: ensure httpd is running 
   service: 
     name: httpd 
     state: started 
 - name: Ouvrir le port 80/TCP 
   firewalld: 
     service: http 
     permanent: true 
     state: enabled 
 - name: Redémarrer le service firewalld service 
   service: 
     name: firewalld 
     state: restarted 
[root@server1 workspace]# 
 

Vérifiez la syntaxe du fichier playbook avec la commande ansible-playbook :

[root@server1 workspace]# ansible-playbook --syntax-check \ 
> httpd.yml 
 
playbook: httpd.yml 
[root@server1 workspace]# 
 

Il est également possible d’utiliser la commande yamllint pour réaliser la vérification syntaxique. Le playbook ne doit pas avoir d’erreur, sinon, corrigez.

4.

Exécutez à blanc le playbook :

[root@server1 workspace]# ansible-playbook -C httpd.yml 
 
PLAY [all] ******************************************************* 
 
TASK [Gathering Facts] ******************************************* 
ok: [10.14.8.1] 
ok: [10.14.8.2] 
 
TASK [Installer les paquetages httpd] **************************** 
changed: [10.14.8.1] 
changed: [10.14.8.2] 
 
TASK [ensure httpd is running] *********************************** 
changed: [10.14.8.1] 
changed: [10.14.8.2] 
 
TASK [Ouvrir le port 80/TCP] ************************************* 
changed: [10.14.8.1] 
changed: [10.14.8.2] 
 
TASK [Redémarrer le service firewalld service] ******************** 
changed: [10.14.8.1] 
changed: [10.14.8.2] 
 
PLAY RECAP ******************************************************* 
10.14.8.1  : ok=5  changed=4  unreachable=0  failed=0  skipped=0  rescued=0   ignored=0 
10.14.8.2  : ok=5  changed=4  unreachable=0  failed=0  skipped=0  rescued=0   ignored=0 
 
[root@server1 workspace]# 
5.

Exécutez le playbook maintenant :

[root@server1 workspace]# ansible-playbook httpd.yml 
 
PLAY [all] ******************************************************* 
 
TASK [Gathering Facts] ******************************************* 
ok: [10.14.8.1] 
ok: [10.14.8.2] 
 
**** sortie tronquée ****  
 

La sortie-écran est similaire à l’exécution à blanc.

Vérifiez que le service httpd est démarré. Connectez-vous depuis la machine server1 sur les hôtes server2 et server3 avec la commande systemctl.

Connexion sur l’hôte server2 :

[root@server1 workspace]# systemctl -H server2 status httpd 
● httpd.service - The Apache HTTP Server 
  Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; 
vendor preset: disabled) 
  Active: active (running) since Mon 2019-12-16 21:08:23 CET; 2h 22min ago 
    Docs: man:httpd.service(8) 
Main PID: 3139 
  Status: "Running, listening on: port 80" 
   Tasks: 213 (limit: 12553) 
  Memory: 24.9M 
  CGroup: /system.slice/httpd.service 
          ├─3139 /usr/sbin/httpd -DFOREGROUND 
          ├─3339 /usr/sbin/httpd -DFOREGROUND 
          ├─3340 /usr/sbin/httpd -DFOREGROUND 
          ├─3341 /usr/sbin/httpd -DFOREGROUND 
          └─3342 /usr/sbin/httpd -DFOREGROUND 
[root@server1 workspace]# 
 

Répétez la même opération pour l’hôte server3 :

[root@server1 workspace]# systemctl -H server3 status httpd 
● httpd.service - The Apache HTTP Server 
  Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; 
vendor preset: disabled) 
 
**** sortie tronquée **** 
2. Déployer et configurer mariaDB

Environnement nécessaire

Vous avez besoin des trois machines virtuelles server1, server2 et server3.

Objectifs du TP

Vous devez déployer sur les hôtes server2 et server3 mariaDB et ouvrir le port dans firewalld.

Tâches à réaliser

1.

Dans le répertoire de travail ~/workspace, vous pouvez réutiliser le fichier inventaire machines.ini du TP précédent.

La résolution de noms d’hôtes n’est pas assumée par un service DNS mais par le fichier /etc/hosts.

Vous allez réutiliser également le fichier ~/workspace/ansible.cfg.

2.

Créez le fichier playbook mariadb.yml :

[root@server1 workspace]# cat mariadb.yml 
--- 
- hosts: all 
  tasks: 
  - name: Installer les paquetages mariadb 
    yum: name=mariadb-server state=latest 
  - name: ensure mariadb is running 
    service: 
      name: mariadb 
      state: started 
  - name: Ouvrir le port 
    firewalld: 
      service: mysql 
      permanent: true 
      state: enabled 
  - name: Redémarrer le service firewalld service 
    service: 
      name: firewalld 
      state: restarted 
[root@server1 workspace]# 
 

Vérifiez la syntaxe du fichier avec la commande ansible-playbook :

[root@server1 workspace]# ansible-playbook --syntax-check \ 
> mariadb.yml 
 
playbook: mariadb.yml 
[root@server1 workspace]# 
 

Le playbook ne doit pas avoir d’erreur, sinon, corrigez.

3.

Exécutez le playbook :

[root@server1 workspace]# ansible-playbook mariadb.yml 
 
PLAY [all] ******************************************************* 
 
TASK [Gathering Facts] ****************************************************************** 
ok: [10.14.8.2] 
ok: [10.14.8.1] 
 
TASK [Installer les paquetages mariadb] ************************** 
changed: [10.14.8.2] 
changed: [10.14.8.1] 
 
TASK [ensure mariadb is running] ********************************* 
changed: [10.14.8.1] 
changed: [10.14.8.2] 
 
TASK [Ouvrir le port] ****************************************************************** 
changed: [10.14.8.1] 
changed: [10.14.8.2] 
 
TASK [Redémarrer le service firewalld service] ******************** 
changed: [10.14.8.2] 
changed: [10.14.8.1] 
 
PLAY RECAP ****************************************************************** 
10.14.8.1  : ok=5  changed=4  unreachable=0  failed=0  skipped=0  rescued=0   ignored=0 
10.14.8.2  : ok=5  changed=4  unreachable=0  failed=0  skipped=0  rescued=0   ignored=0 
 
[root@server1 workspace]# 
4.

Exécutez une commande ad hoc pour vérifier que les paquets de mariaDB sont bien installés :

[root@server1 workspace]# ansible all -m shell \ 
> -a 'rpm -qa mariadb*' 
[WARNING]: Consider using the yum, dnf or zypper module rather than running 
'rpm'.  If you need to use command 
because yum, dnf or zypper is insufficient you can add 'warn: false' to this 
command task or set 
'command_warnings=False' in ansible.cfg to get rid of this message. 
 
10.14.8.1 | CHANGED | rc=0 >> 
mariadb-server-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-common-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-backup-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-connector-c-3.0.7-1.el8.x86_64 
mariadb-gssapi-server-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-errmsg-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-server-utils-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-connector-c-config-3.0.7-1.el8.noarch 
mariadb-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
 
10.14.8.2 | CHANGED | rc=0 >> 
mariadb-server-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-common-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-backup-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-connector-c-3.0.7-1.el8.x86_64 
mariadb-gssapi-server-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-errmsg-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-server-utils-10.3.11-2.module_el8.0.0+35+6f2527ed.x86_64 
mariadb-connector-c-config-3.0.7-1.el8.noarch 
 
[root@server1 workspace]# 
5.

Exécutez une commande ad hoc pour vérifier que le service mariadb est bien démarré :

[root@server1 workspace]# ansible all -m service \ 
> -a 'name=mariadb state=started' 
10.14.8.1 | SUCCESS => { 
   "ansible_facts": { 
       "discovered_interpreter_python": "/usr/libexec/platform-python" 
   }, 
   "changed": false, 
   "name": "mariadb", 
   "state": "started", 
   "status": { 
       "ActiveEnterTimestamp": "Tue 2019-12-17 01:29:45 CET", 
       "ActiveEnterTimestampMonotonic": "24826341473", 
       "ActiveExitTimestampMonotonic": "0", 
       "ActiveState": "active", 
 
**** sortie tronquée **** 
 
10.14.8.2 | SUCCESS => { 
   "ansible_facts": { 
       "discovered_interpreter_python": "/usr/libexec/platform-python" 
   }, 
   "changed": false, 
   "name": "mariadb", 
   "state": "started", 
   "status": { 
       "ActiveEnterTimestamp": "Tue 2019-12-17 01:29:44 CET", 
       "ActiveEnterTimestampMonotonic": "24823480851", 
       "ActiveExitTimestampMonotonic": "0", 
       "ActiveState": "active", 
 
**** sortie tronquée **** 
 

Vérifiez aussi que le port est ouvert dans le firewall avec une commande ad hoc.

[root@server1 workspace]# ansible all -m shell \ 
> -a 'firewall-cmd --list-service' 
10.14.8.2 | CHANGED | rc=0 >> 
cockpit dhcpv6-client http mysql ssh 
 
10.14.8.1 | CHANGED | rc=0 >> 
cockpit dhcpv6-client http mysql ssh 
 
[root@server1 workspace]# 
